package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

/*
* testing given url in both correct and wrong form
* based on https://github.com/marni/imt2681_studentdb/blob/master/api_student_test.go
 */
func TestGivenURL(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(handler))
	defer ts.Close()

	testCases := []string{
		ts.URL + "/projectinfo/v1/github.com/golang/go",
		ts.URL + "/projectinfo/v1/github.com/golang/go/",
		ts.URL + "/projectinfo/v1/golang/go",
		ts.URL + "/projectinfo/v1/golang/go/",
	}
	for _, tstring := range testCases {
		resp, err := http.Get(tstring)
		if err != nil {
			t.Errorf("Error making the GET request, %s", err)
		}

		if resp.StatusCode == http.StatusBadRequest {
			t.Errorf("For route: %s, expected StatusCode %d, received %d", tstring,
				http.StatusBadRequest, resp.StatusCode)
			return
		}
	}
	testCasesErr := []string{
		ts.URL,
		ts.URL + "/projectinfo/v1",
		ts.URL + "/projectinfo/v1/",
		ts.URL + "/projectinfo/v1/XXX",
		ts.URL + "/projectinfo/v1/githubXXX.com/golang/go",
		ts.URL + "/projectinfo/v1/github.com/golang/go/XXX",
		//		ts.URL + "/golang/go/",
	}
	for _, tstring := range testCasesErr {
		resp, err := http.Get(tstring)
		if err != nil {
			t.Errorf("Error making the GET request, %s", err)
		}

		if resp.StatusCode != http.StatusBadRequest {
			t.Errorf("For route: %s, expected StatusCode %d, received %d", tstring,
				http.StatusBadRequest, resp.StatusCode)
			return
		}
	}

}
